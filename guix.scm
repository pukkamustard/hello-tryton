(use-modules (guix packages)
	     (guix build-system python)
	     (guix build-system gnu)
	     (gnu packages python)
	     (gnu packages python-xyz)
	     (gnu packages sqlite)
	     (gnu packages tryton))

(package
 (name "hello-tryton")
 (version "0.0.0")
 (source #f)
 (build-system gnu-build-system)
 (propagated-inputs
  (list
   python
   python-pip
   tryton ; the GTK client
   sqlite))
 (description #f)
 (synopsis #f)
 (home-page #f)
 (license #f))

