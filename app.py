from flask import Flask


# Connect with Tryton using proteus
import proteus

# setup config file and database to use
proteus.config.set_trytond(config_file='./config', database='test')

User = proteus.Model.get('res.user')

# Flask App
app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"
